// Empty JS for your own code to be here
$ ( document ).ready ( function () {

    let current_url_path = $ ( location ).attr ( 'href' );
    let url = new URL ( current_url_path );
    let error = url.searchParams.get ( "error" );
    /**
     * Jquery hide New and existing customer form.
     */
   // $ ( '#div-nw-cus' ).hide ();
    //$ ( '#div-ex-cus' ).hide ();
    $ ( '.checkButton' ).hide ();
    $ ( '#div-wb-lst-tst' ).hide ();
    /**
     * Jquery newsletter webhook hide
     */
    $ ( '#nws-card' ).hide ();
    $ ( '#ecm-card' ).hide ();

    /**
     * Jquery hide test and save buttons
     */
    function clearAlertMessage(className) {
        $ ( '.' + className ).empty ().removeClass ( 'alert-success' ).removeClass ( 'alert-danger' ).addClass ( 'text-hide' );
    }

    /**
     * Test NewsLetter Webhook
     */
    $ ( '#tst-nws-wbh' ).click ( function () {
        let nws = $ ( '#sel-gt-nws-wb' ).val ();
        if ( !storeApiKeyAndAccNoToLocalStorage () || nws === 'default' ) {
            $ ( '.alert-cioapikey' ).append ( 'Please select a NewsLetter Webhook from the list above' ).removeClass ( 'text-hide' );
            return false;
        }
        // redirect to newsletter webhook.
        window.location.href = 'https://online-mehr-geschaeft.de/treaction/msShopPlugin/newsLetterWebhook/' + window.location.search;

    } );

    /**
     * Test Ecommerce Webhook
     */
    $ ( '#tst-ecm-wbh' ).click ( function () {
        let ecm = $ ( '#sel-gt-ecm-wb' ).val ();
        if ( !storeApiKeyAndAccNoToLocalStorage () || ecm === 'default' ) {
            $ ( '.alert-cioapikey' ).append ( 'Please select eCommerce Webhook from the list above' ).removeClass ( 'text-hide' );
            return false;
        }
        // redirect to eCommerce webhook.
        window.location.href = 'https://online-mehr-geschaeft.de/treaction/msShopPlugin/eCommerceWebhook/' + window.location.search;
    } );

    /**
     *  Storing APIKey and AccountNumber to LocalStorage
     */
    function storeApiKeyAndAccNoToLocalStorage() {
        clearAlertMessage ( 'alert-cioapikey' );
        let accNo = $ ( '#tst-acc-no' ).val ();
        let apikey = $ ( '#tst-apikey' ).val ();
        if ( !accNo || !apikey ) {
            $ ( '.alert-cioapikey' ).append ( 'APIkey or Account Number is missing' ).removeClass ( 'text-hide' );
            return false;
        }
        localStorage.setItem ( "apikey", apikey );
        localStorage.setItem ( "accountNo", accNo );

        return true;
    }

    /**
     * Loader Webhook for fetching Webhooks from AIO
     */
    $ ( '#row-gt-wb-lst-loader' ).hide ();

    /**
     * Jquery for showing Wehooks
     */
    $ ( '#div-wb-lst' ).hide ();

    // selecting newsletter webhook
    $ ( '#sel-gt-nws-wb' ).change ( function () {
        let val = $ ( '#sel-gt-nws-wb' ).val ();
        localStorage.setItem ( 'objRegId', val );
        // $ ( '#sel-gt-ecm-wb' ).val ( 'default' );
    } );

    // selecting ecommerce webhook
    $ ( '#sel-gt-ecm-wb' ).change ( function () {
        let val = $ ( '#sel-gt-ecm-wb' ).val ();
        localStorage.setItem ( 'objRegId', val );
        // $ ( '#sel-gt-nws-wb' ).val ( 'default' );
    } );

    /**
     * Jquery for saving webhook configurations
     */
    $ ( '#bt-sv-cnf-wbhk' ).hide ();

    /**
     * Jquery for Generating a new API
     */
    $ ( '#div-cus-sts' ).hide ();
    $ ( '#bt-gt-apikey' ).click ( function (e) {
        $ ( '#div-cus-sts' ).fadeIn ();
    } );

    /**
     * Showing appropriate form for Existing / New Customer.
     */
/*    $ ( '#sel-cus-st-optin' ).change ( function (e) {
        let selOpt = $ ( '#sel-cus-st-optin' ).val ();
        if ( selOpt === 'nw-cus' ) {
            $ ( '#div-ex-cus' ).hide ();
            $ ( '#div-nw-cus' ).fadeIn ();
        } else if ( selOpt === 'ex-cus' ) {
            $ ( '#div-nw-cus' ).hide ();
            $ ( '#div-ex-cus' ).fadeIn ();
        }
    } );*/

    /**
     * Show or hide appropriate error messages.
     */
    if ( error ) {
        $ ( '.alert-cioapikey' ).append ( atob ( error ) ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
    }
    let success = url.searchParams.get ( "success" );
    if ( success ) {
        $ ( '.alert-cioapikey' ).append ( atob ( success ) ).addClass ( 'alert-success ' ).removeClass ( 'text-hide' );
    }

    /**
     * ASCII to Unicode (decode Base64 to original data)
     * @param {string} b64
     * @return {string}
     */
    function atou(b64) {
        return decodeURIComponent ( escape ( atob ( b64 ) ) );
    }

    /**
     * Unicode to ASCII (encode data to Base64)
     * @param {string} data
     * @return {string}
     */
    function utoa(data) {
        return btoa ( unescape ( encodeURIComponent ( data ) ) );
    }

    /**
     * Test APIKey Button and get list of webhooks
     */
    $ ( '#bt-tst-cnf' ).click ( function (e) {
        // $("#row-gt-wb-lst-loader").show();
        $ ( '.alert-cioapikey' ).empty ().removeClass ( 'alert-success' ).removeClass ( 'alert-danger' ).addClass ( 'text-hide' );
        $ ( '#sel-gt-ecm-wb' ).empty ();
        $ ( '#sel-gt-nws-wb' ).empty ();
        // fetch acc_no and apikey for testing.
        let accNo = $ ( '#tst-acc-no' ).val ();
        let apikey = $ ( '#tst-apikey' ).val ();
        if ( !accNo || !apikey ) {
            // show error for account no and apikey.
            return false;
        }
        let formData = {
            "base": {
                'account_no': accNo,
                'apikey': apikey
            }
        };
        let formJson = JSON.stringify ( formData );
        let formbtoa = btoa ( formJson );

        $.ajax ( {
            url: "https://dev-api-in-one.net/webhook/list/read",
            type: 'POST',
            async: false,
            timeout: 0,
            header: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
                'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
                'Access-Control-Max-Age': '86400'
            },
            data: {data: formbtoa},
            success: function (data) {
                let response = ( data );
                console.log(response);
                console.log(response.status);
                if ( response.status ) {
                    $ ( '#div-wb-lst-tst' ).show ();
                    $ ( '#div-wb-lst' ).show ();
                    $ ( '#sel-gt-wb' ).show ();
                    $ ( '#bt-sve-cnf' ).show ();
                    $ ( "#row-gt-wb-lst-loader" ).hide ();
                    let options = response.response;
                    console.log ('options', options );
                    $ ( '#sel-gt-ecm-wb' ).append ( '<option value="default" selected>Please select eCommerce Webhook</option>' );
                    $ ( '#sel-gt-nws-wb' ).append ( '<option value="default" selected>Please select NewsLetter Webhook</option>' )
                    $.each ( options, function (key, value) {
                        $ ( '.checkButton' ).css ( {'color': '#16eb1e'} ).show ();
                        $ ( '#sel-gt-ecm-wb' ).append ( $ ( '<option value=' + value['objectregister_id'] + '>' + value['name'] + '</option>' ) );
                        $ ( '#sel-gt-nws-wb' ).append ( $ ( '<option value=' + value['objectregister_id'] + '>' + value['name'] + '</option>' ) );
                    } );
                    $ ( '.alert-cioapikey' ).append ( response.message ).removeClass ( 'text-hide' );
                } else {
                    $ ( "#row-gt-wb-lst-loader" ).hide ();
                    $ ( '#sel-gt-nws-wb' ).show ();
                    $ ( '.checkButton' ).css ( {'color': '#eb101f'} ).show ();
                    $ ( '.alert-cioapikey' ).append ( response.message ).removeClass ( 'text-hide' );
                }
            },
            error: function (data) {
                $ ( '.checkButton' ).css ( {'color': '#eb101f'} );
            }
        } );
        // Accessing for future use.
        // For eCommerce and newsletter webhooks.
        localStorage.setItem ( "apikey", apikey );
        localStorage.setItem ( "accountNo", accNo );
        return false;
    } );

    /**
     * Jquery Generate APIKey for existing customer.
     */
    $ ( '#bt-snd-apikey' ).click ( function (e) {
        $ ( '.alert-ex-cus' ).empty ().removeClass ( 'alert-success' ).removeClass ( 'alert-danger' ).addClass ( 'text-hide' );
        let accNo = $ ( '#ex-cus-acc-id' ).val ();
        if ( !accNo ) {
            // show error for invalid account NO.
            return false;
        }
        let formData = [{'accountNo': accNo}];
        let formJson = JSON.stringify ( formData );
        let formbtoa = utoa( formJson );

        $.ajax ( {
            url: "https://dev-api-in-one.net/general/account/apikey/create",
            type: 'POST',
            async: false,
            timeout: 0,
            header: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
                'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
                'Access-Control-Max-Age': '86400'
            },
            data: formbtoa,
            beforeSend: function (xhr) {
                $ ( '.alert-ex-cus' ).removeClass ( 'alert-success' ).removeClass ( 'alert-danger' ).addClass ( 'text-hide' );
            },
            success: function (data) {
                let response = ( data );
                if ( response.status ) {
                    //$ ( '.btn-submit' ).hide ();
                    $ ( '.alert-ex-cus' ).append ( response.message ).addClass ( 'alert-success ' ).removeClass ( 'text-hide' );
                } else {
                    //$ ( '.btn-submit' ).hide ();
                    $ ( '.alert-ex-cus' ).append ( response.message ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
                }
            }
        } );
        return false;
    } );


    /**
     * Jquery Generate APIKey for new customer
     */
    $ ( '#bt-snd-nw-apikey' ).click ( function (e) {
        $ ( '.alert-nw-cus' ).empty ().removeClass ( 'alert-success' ).removeClass ( 'alert-danger' ).addClass ( 'text-hide' );
        //$ ( '#bt-snd-nw-apikey' ).hide ();
        let salutation = $ ( '#nw-cus-sal' ).val ();
        let firstname = $ ( '#nw-cus-fstnme' ).val ();
        let lastname = $ ( '#nw-cus-lstnme' ).val ();
        let email = $ ( '#nw-cus-em' ).val ();
        let permission = $ ( '#nw-cus-permission' ).is ( ':checked' );

        let formData = {
            'salutation': salutation,
            'firstName': firstname,
            'lastName': lastname,
            'email': email,
            'permission': permission,
        };

        if ( !email ) {
            $ ( '.alert-nw-cus' ).append ( 'Bitte geben Sie den E-Mail ein' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( '#nw-cus-em' ).focus ();
            $ ( '#bt-snd-nw-apikey' ).show ();
            return false;
        }
        if ( !firstname ) {
            $ ( '.alert-nw-cus' ).append ( 'Bitte geben Sie den Vorname ein' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( '#nw-cus-fstnme' ).focus ();
            $ ( '#bt-snd-nw-apikey' ).show ();
            return false;
        }
        if ( !lastname ) {
            $ ( '.alert-nw-cus' ).append ( 'Bitte geben Sie den Nachname ein' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( '#nw-cus-lstnme' ).focus ();
            $ ( '#bt-snd-nw-apikey' ).show ();
            return false;
        }

        if ( !permission ) {
            $ ( '.alert-nw-cus' )
                .append ( 'Bitte akzeptieren Sie die Allgemeinen Geschäftsbedingungen und Datenschutzbestimmungen' )
                .addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( '#nw-cus-permission' ).focus ();
            $ ( '#bt-snd-nw-apikey' ).show ();
            return false;
        }

        let formJson = JSON.stringify ( formData );
        let formbtoa = btoa ( formJson );

        $.ajax ( {
            url: "https://dev-api-in-one.net/general/account/create",
            type: 'POST',
            async: false,
            timeout: 0,
            header: {
                //'Content-Type':'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
                'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
                'Access-Control-Max-Age': '86400'
            },
            data: formbtoa,
            beforeSend: function (xhr) {
                $ ( '.alert-nw-cus' ).removeClass ( 'alert-success' ).removeClass ( 'alert-danger' ).addClass ( 'text-hide' );
            },
            success: function (data) {
                let response = ( data );
                if ( response.status ) {
                    //$ ( '.btn-submit' ).hide ();
                    $ ( '.alert-nw-cus' ).append ( response.message ).addClass ( 'alert-success ' ).removeClass ( 'text-hide' );
                } else {
                    //$ ( '.btn-submit' ).hide ();
                    $ ( '.alert-nw-cus' ).append ( response.message ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
                }
            }
        } );
        return false;
    } );
} );
