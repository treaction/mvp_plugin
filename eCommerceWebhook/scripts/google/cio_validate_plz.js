/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    var address = "";
    var zip = "";
    var ort = "";
    
    $('.error_postal_code').hide();
    $('.error_address').hide();
    
    $("input[name=adresse]").blur(function () {

        var val = $(this).val();
        
        address = (val.trim() && val.length > 0) ? val.split(" ").join("+") : "";
  
        if(address.length > 0 && zip.length > 0 && ort.length > 0){
            validateAddressWithPostalcode();
        }    
    });
    
    
    
    $("input[name=postal_code]").blur(function () {
        
        var val = $(this).val();
        var city;
        $('.errorplz').hide();
        // Check for autofill postal code value
            ort = checkZipCode(val);
            if (ort.length > 0) {
                $('.city').val(ort);
            } else {
                $('.city').val("");
                $('.errorplz').show();
                $("input[type=submit]").hide();
            }

        
        zip = (val.length > 0) ? val : "";

        if(zip.length > 0 && ort.length > 0 && address.length > 0){
            validateAddressWithPostalcode();
        }    
    });
    
    
    
    function validateAddressWithPostalcode(){
       
        var gurl = "";
        var status = false;
        if(address.length > 0 && zip.length > 0 && ort.length > 0){
            gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+ort+'&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';
            $.getJSON({
                url: gurl,
                async: false,
                success: function (response, textStatus) {
                    // check the status of the request
                    if (response.status == "ZERO_RESULTS") {
                        status = false; // Postal code not found or wrong postal code.
                    } else {        
                        // Postal code is found
                        var address_components = response.results[0].address_components;
                        $.each(address_components, function (index, component) {
                            var types = component.types;
                            // Find the city for the postal code
                            $.each(types, function (index, type) {
                                if (type == 'postal_code') {
                                    if(component.long_name == zip){
                                        status = true;
                                    } 
                                }
                            });
                        });                        
                    } 
                }
            });    
        }
        else{
            status = false;
        }
        
        if(status == false){
            $('.error_address').show();
            $("input[type=submit]").hide();
        }else{
            $('.error_address').hide();
            $("input[type=submit]").show();
        }
    }
   


    /* Function Name : checkZipCode
     * Parameters : zipcode
     * Returns : 
     *    1. countryName on success .
     *    2. False on faiure.
     */

    function checkZipCode(zip) {
        var status = true;
        var city;
        if (zip.length == 5 ) {
            var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=Germany' + zip + '&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';
            $.getJSON({
                url: gurl,
                async: false,
                success: function (response, textStatus) {
                    // check the status of the request
                    if (response.status !== "OK") {
                        status = false; // Postal code not found or wrong postal code.
                    } else {
                        // Postal code is found
                        status = true
                        var address_components = response.results[0].address_components;
                        $.each(address_components, function (index, component) {
                            var types = component.types;
                            // Find the city for the postal code
                            $.each(types, function (index, type) {
                                if (type == 'locality') {
                                    city = component.long_name;
                                    status = true;
                                }
                            });
                        });
                    }
                }
            });
        } else {
            status = false;
        }
        if (status) {
            return city;
        } else {
            return false;
        }
    }
});