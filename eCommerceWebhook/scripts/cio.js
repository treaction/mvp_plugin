/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$ ( document ).ready ( function () {

    // Error messages;
    $ ( '.error_aio' ).hide ();
    $ ( '.error_success' ).hide ();
    $ ( '.error_postal_code' ).hide ();
    $ ( '.error_address' ).hide ();


 //   $ ( '#ecm-card' ).hide ();

    /**
     * Loader Webhook for fetching Webhooks from AIO
     */
    $ ( '#row-gt-wb-lst-loader' ).hide ();

    /**
     * Hidden fields
     */
    $('#fld-hidden').hide();
    $('#hide-hidden-flds').hide();
    $('#shw-hidden-flds').click(function() {
        $('#fld-hidden').show();
        $('#shw-hidden-flds').hide();
        $('#hide-hidden-flds').show();
    });

    $('#hide-hidden-flds').click(function() {
        $('#shw-hidden-flds').show();
        $('#hide-hidden-flds').hide();
        $('#fld-hidden').hide();
    });

    // selecting ecommerce webhook
    $ ( '#sel-gt-ecm-wb' ).change ( function () {
        $ ( '#card-gt-apikey' ).fadeOut ();
        $ ( '#nws-card' ).fadeOut ();
        $ ( '#sel-gt-nws-wb' ).val ();
        $ ( '#ecm-card' ).fadeIn ();
    } );

    /**
     * Jquery for Ecommerce Webhook Submit
     */
    $ ( '#ems-wb-submit' ).click ( function () {
        $('#ems-wb-submit').val('Please wait while we process your order.');
        $ ( '.alert-ex-cus' ).empty ().removeClass ( 'alert-success' ).removeClass ( 'alert-danger' ).addClass ( 'text-hide' );
        // Account Parameters.
        let apikey = localStorage.getItem('apikey');
        let accNo = localStorage.getItem('accountNo');
        let objRegId = localStorage.getItem('objRegId');

        //Url params.
        let refId = $ ( '#ems-wb-refId' ).val ();
        let netOrder = $ ( '#ems-wb-netval' ).val ();
        let affId = $ ( '#ems-wb-affsubid' ).val ();
        let affSubId = $ ( '#ems-wb-affid' ).val ();
        let url = $ ( '#ems-wb-urlparm' ).val ();

        let salutation = $ ( "input[name ='anrede']:checked" ).val ();
        let firstname = $ ( '#ems-wb-fstname' ).val ();
        let lastname = $ ( '#ems-wb-nachname' ).val ();
        let address = $ ( '#ems-wb-addr' ).val ();
        let hno = $ ( '#ems-wb-hno' ).val ();
        let plz = $ ( '#ems-wb-plz' ).val ();
        let city = $ ( '#ems-wb-city' ).val ();
        let country = $ ( '#ems-wb-cnty' ).val ();
        let email = $ ( '#ems-wb-eml' ).val ();
        let date = $ ( '#ems-wb-date' ).val ();
        let productCat = $ ( '#ems-wb-pdt-cat' ).val ();
        let orderno = $ ( '#ems-wb-ordno' ).val ();
        let last_year_order_net_value = $ ( '#ems-wb-lyonv' ).val ();
        let last_order_net_value = $ ( '#ems-wb-lonv' ).val ();
        let total_order_net_value = $ ( '#ems-wb-tonv' ).val ();
        let permission = $('#ems-agree-tc').is(':checked');
        // Check for Mandatory fields
        if(!salutation) {
            $ ( '.alert-ex-cus' ).append ( 'Please provide the Salutation' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( "input[name ='anrede']").focus();
            $ ( '#ems-wb-submit' ).show();
            return false;
        }
        if(!email) {
            $ ( '.alert-ex-cus' ).append ( 'Please provide the E-Mail address' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( '#ems-wb-eml' ).focus();
            $ ( '#ems-wb-submit' ).show();
            return false;
        }
        if(!firstname) {
            $ ( '.alert-ex-cus' ).append ( 'Please provide your FirstName' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( '#ems-wb-fstname' ).focus();
            $ ( '#ems-wb-submit' ).show();
            return false;
        }
        if(!lastname) {
            $ ( '.alert-ex-cus' ).append ( 'Please provide your Lastname' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( '#ems-wb-nachname' ).focus();
            $ ( '#ems-wb-submit' ).show();
            return false;
        }
        if(!orderno) {
            $ ( '.alert-ex-cus' ).append ( 'Please provide the Order Number' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( '#ems-wb-ordno' ).focus();
            $ ( '#ems-wb-submit' ).show();
            return false;
        }
        if(!date) {
            $ ( '.alert-ex-cus' ).append ( 'Please provide the Order Date' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( '#ems-wb-date' ).focus();
            $ ( '#ems-wb-submit' ).show();
            return false;
        }
        if(!productCat) {
            $ ( '.alert-ex-cus' ).append ( 'Please provide the Product categories saperated by comma ","' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            $ ( '#ems-wb-pdt-cat' ).focus();
            $ ( '#ems-wb-submit' ).show();
            return false;
        }


        let formdata = {
            "base": {"apikey": apikey, "account_number": accNo, "object_register_id": objRegId},
            "contact": {
                "standard": [
                    {"salutation": salutation, "required": "true", "datatype": "Text", "regex": ""},
                    {"firstname": firstname, "required": "true", "datatype": "Text", "regex": ""},
                    {"lastname": lastname, "required": "true", "datatype": "Text", "regex": ""},
                    {"email": email, "required": "true", "datatype": "Text", "regex": ""},
                    {"street": address, "required": "true", "datatype": "Text", "regex": ""},
                    {"house_number": hno, "required": "true", "datatype": "Text", "regex": ""},
                    {"postal_code": plz, "required": "true", "datatype": "Text", "regex": ""},
                    {"city": city, "required": "true", "datatype": "Text", "regex": ""},
                    {"country": country, "required": "true", "datatype": "Text", "regex": ""},
                    {"last_order_date": date, "required": "true", "datatype": "Text", "regex": ""},
                    {"smart_tags": productCat, "required": "true", "datatype": "Text", "regex": ""},
                    {"last_order_no": orderno, "required": "true", "datatype": "Text", "regex": ""},
                    {"last_order_net_value": last_order_net_value, "required": "true", "datatype": "Text", "regex": ""},
                    {"total_order_net_value": total_order_net_value, "required": "true", "datatype": "Text", "regex": ""},
                    {"last_year_order_net_value": last_year_order_net_value, "required": "true", "datatype": "Text", "regex": ""},
                    {"referrer_id": refId, "required": "true", "datatype": "Text", "regex": ""},
                    {"affiliate_id": affId, "required": "true", "datatype": "Text", "regex": ""},
                    {"affiliate_sub_id": affSubId, "required": "true", "datatype": "Text", "regex": ""},
                    {"url": url, "required": "true", "datatype": "Text", "regex": ""}

                ]
            }
        }
        let formJson = JSON.stringify ( formdata );
        let formbtoa = utoa ( formJson );


        $.ajax ( {
            url: "https://dev-api-in-one.net/webhook/receive",
            type: 'POST',
            async: false,
            timeout: 0,
            header: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
                'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
                'Access-Control-Max-Age': '86400'
            },
            data: formbtoa,
            success: function (data) {
                let response = ( data );
                if ( response.status ) {
                    $('#ems-wb-submit').val('Your order has been placed successfully.');
                    $ ( '.alert-ex-cus' ).append ( response.message ).addClass ( 'alert-success ' ).removeClass ( 'text-hide' );
                } else {
                    $('#ems-wb-submit').val('Something went wront! Unable to place your order, Please contact our Customer Care.');
                    $ ( '.alert-ex-cus' ).append ( response.message ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
                }
            }
        } );

        console.log ( formbtoa );
        return false;
    } );


    /**
     * Fetching All the url parameters;
     */
    let refId = getUrlVars ()['referrerId'];
    let netOrder = getUrlVars ()['netOrderValue'];
    let affId = getUrlVars ()['affiliateId'];
    let affSubId = getUrlVars ()['affiliateSubId'];
    let urlParm = $ ( location ).attr ( 'href' );

    let apikey = getUrlVars ()['apikey'];
    let accNo = getUrlVars ()['acc_id'];
    let objRegId = getUrlVars ()['objrefid'];

    $ ( '#tst-apikey' ).val ( apikey );
    $ ( '#tst-acc-no' ).val ( accNo );
    $ ( '#tst-objrefid-no' ).val ( objRegId );
    $ ( '#ems-wb-refId' ).val ( refId );
    $ ( '#ems-wb-netval' ).val ( netOrder );
    $ ( '#ems-wb-affsubid' ).val ( affId );
    $ ( '#ems-wb-affid' ).val ( affSubId );
    $ ( '#ems-wb-urlparm' ).val ( urlParm );

    /**
     * Get URL parameters
     * @returns {{}}
     */
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace ( /[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        } );
        return vars;
    }

    /**
     * Fetch information about the device.
     * @returns {[]}
     */
    function getDeviceInformation() {
        let info = [];
        let deviceInstance = $.fn.deviceDetector;
        let deviceInfo = deviceInstance.getInfo ();
        if ( deviceInfo ) {
            info['device_type'] = deviceInfo.mobile ? 'mobile' : 'desktop';
            info['device_browser'] = deviceInfo.browserName;
            info['device_os'] = deviceInfo.osName;
            info['device_os_version'] = deviceInfo.osVersion;
        }

        return info;
    }


    /**
     * Get UTM Parameters from URL
     */
    function getUTMParameters() {
        // beispiel : utm_term=fibo&utm_medium=adwords&utm_source=google&utm_campaign=emsfibopreis49
        let utm = ['utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content'];
        let utm_parameter = '';
        $.each ( utm, function (index, value) {
            let temp_var = getUrlVars ()[value];
            if ( temp_var ) {
                utm_parameter += value + '=' + temp_var + '&';
            }
        } );
        return utm_parameter;
    }


    /**
     * ASCII to Unicode (decode Base64 to original data)
     * @param {string} b64
     * @return {string}
     */
    function atou(b64) {
        return decodeURIComponent(escape(atob(b64)));
    }

    /**
     * Unicode to ASCII (encode data to Base64)
     * @param {string} data
     * @return {string}
     */
    function utoa(data) {
        return btoa(unescape(encodeURIComponent(data)));
    }
} );
