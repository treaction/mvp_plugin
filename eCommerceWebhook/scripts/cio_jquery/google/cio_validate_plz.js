/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    var address = "";
    var zip = "";
    var ort = "";
    
    $('.errorplz').hide();
    $('.erroradress').hide();    
    
    $("input[name='"+mapping_strasse+"']").blur(function () {

        var val = $(this).val();
        
        address = (val.trim() && val.length > 0) ? val.split(" ").join("+") : "";
  
        if(address.length > 0 && zip.length > 0 && ort.length > 0 && address_validation){
            validateAddressWithPostalcode();
        }    
    });
    
    
    
    $("input[name=plz]").blur(function () {
        
        var val = $(this).val();
        var city;
        $('.errorplz').hide();
        // Check for autofill postal code value
        if (autofill_postal_code ) {
            ort = checkZipCode(val);
            if (ort.length > 0) {
                $('.ort').val(ort);
            } else {
                $('.ort').val("");
                $('.errorplz').show();
            }
        }        
        
        zip = (val.length > 0) ? val : "";

        if(zip.length > 0 && ort.length > 0 && address.length > 0 && address_validation){
            validateAddressWithPostalcode();
        }    
    });
    
    
    
    function validateAddressWithPostalcode(){
       
        var gurl = "";
        var status = false;
        if(address.length > 0 && zip.length > 0 && ort.length > 0){
            gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+ort+'&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';
            $.getJSON({
                url: gurl,
                async: false,
                success: function (response, textStatus) {
                    // check the status of the request
                    if (response.status == "ZERO_RESULTS") {
                        status = false; // Postal code not found or wrong postal code.
                    } else {        
                        // Postal code is found
                        var address_components = response.results[0].address_components;
                        $.each(address_components, function (index, component) {
                            var types = component.types;
                            // Find the city for the postal code
                            $.each(types, function (index, type) {
                                if (type == 'postal_code') {
                                    if(component.long_name == zip){
                                        status = true;
                                    } 
                                }
                            });
                        });                        
                    } 
                }
            });    
        }
        else{
            status = false;
        }
        
        if(status == false){
            $('.erroradress').show();
        }else{
            $('.erroradress').hide();
        }
    }
   
   
    /* JQuery Blur event for the postal code
     * Parameters : 
     * Returns : 
     *    1. fill the city Name on success.
     */
//    $('.plz').blur(function () {
//        zip = $(this).val();
//        var city;
//        $('.errorplz').hide();
//        // Check for autofill postal code value
//        if (autofill_postal_code ) {
//            city = checkZipCode(zip);
//            if (city) {
//                $('.ort').val(city);
//            } else {
//                $('.ort').val("");
//                $('.errorplz').show();
//            }
//        }
//    });



    /* Function Name : checkZipCode
     * Parameters : zipcode
     * Returns : 
     *    1. countryName on success .
     *    2. False on faiure.
     */

    function checkZipCode(zip) {
        var status = true;
        var city;
        if (zip.length == 5 && localization_postal_code) {
            var gurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + localization_postal_code + '+' + zip + '&key=AIzaSyDA10Y_CEIbkz2OY-Zp7PsBBjKB5YNh77I';
            $.getJSON({
                url: gurl,
                async: false,
                success: function (response, textStatus) {
                    // check the status of the request
                    if (response.status !== "OK") {
                        status = false; // Postal code not found or wrong postal code.
                    } else {
                        // Postal code is found
                        status = true
                        var address_components = response.results[0].address_components;
                        $.each(address_components, function (index, component) {
                            var types = component.types;
                            // Find the city for the postal code
                            $.each(types, function (index, type) {
                                if (type == 'locality') {
                                    city = component.long_name;
                                    status = true;
                                }
                            });
                        });
                    }
                }
            });
        } else {
            status = false;
        }
        if (status) {
            return city;
        } else {
            return false;
        }
    }
});