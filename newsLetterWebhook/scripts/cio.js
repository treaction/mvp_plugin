/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$ ( document ).ready ( function () {

    // Error messages;
    $ ( '.error_aio' ).hide ();
    $ ( '.error_success' ).hide ();
    $ ( '#div-wb-lst' ).hide ();
    $ ( '#col-sel-gt-web' ).hide ();
    $ ( '#row-gt-wb-lst-loader' ).hide ();


    /**
     * Hidden fields
     */
    $ ( '#fld-hidden' ).hide ();
    $ ( '#hide-hidden-flds' ).hide ();
    $ ( '#shw-hidden-flds' ).click ( function () {
        $ ( '#fld-hidden' ).show ();
        $ ( '#shw-hidden-flds' ).hide ();
        $ ( '#hide-hidden-flds' ).show ();
    } );

    $ ( '#hide-hidden-flds' ).click ( function () {
        $ ( '#shw-hidden-flds' ).show ();
        $ ( '#hide-hidden-flds' ).hide ();
        $ ( '#fld-hidden' ).hide ();
    } );


    /**
     * Fetching All the url parameters;
     */
    let urlParm = $ ( location ).attr ( 'href' );
    let apikey = getUrlVars ()['apikey'];
    let accNo = getUrlVars ()['acc_id'];
    let objRegId = getUrlVars ()['objrefid'];

    $ ( '#tst-apikey' ).val ( apikey );
    $ ( '#tst-acc-no' ).val ( accNo );
    $ ( '#tst-objrefid-no' ).val ( objRegId );
    $ ( '#ems-wb-urlparm' ).val ( urlParm );

    /**
     * Get URL parameters
     * @returns {{}}
     */
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace ( /[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        } );
        return vars;
    }


    /**
     * ASCII to Unicode (decode Base64 to original data)
     * @param {string} b64
     * @return {string}
     */
    function atou(b64) {
        return decodeURIComponent ( escape ( atob ( b64 ) ) );
    }

    /**
     * Unicode to ASCII (encode data to Base64)
     * @param {string} data
     * @return {string}
     */
    function utoa(data) {
        return btoa ( unescape ( encodeURIComponent ( data ) ) );
    }

    /**
     * Jquery for NewsLetter Webhook Submit
     */
    $ ( '#nws-submit' ).click ( function () {
        $ ( '#nws-submit' ).val ( 'Please wait while we process your subscription' );
        $ ( '.alert-ex-cus' ).empty ().removeClass ( 'alert-success' ).removeClass ( 'alert-danger' ).addClass ( 'text-hide' );

        // Account Parameters.
        let apikey = localStorage.getItem ( 'apikey' );
        let accNo = localStorage.getItem ( 'accountNo' );
        let objRegId = localStorage.getItem ( 'objRegId' );

        let salutation = $ ( "input[name ='anrede']:checked" ).val ();
        let firstname = $ ( '#nws-fstname' ).val ();
        let lastname = $ ( '#nws-lstnme' ).val ();
        let email = $ ( '#nws-em' ).val ();
        let optin_txt_1 = $ ( '#optin-txt-1' ).is ( ':checked' );

        // Check for Mandatory fields
        if ( !email ) {
            $ ( '.alert-ex-cus' ).append ( 'Please fill the E-Mail address before submitting the form' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            return false;
        }
        if ( !optin_txt_1 ) {
            $ ( '.alert-ex-cus' ).append ( 'Please accept our Terms and Conditions' ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
            return false;
        }
        let formData = {
            "base": {"apikey": apikey, "account_number": accNo, "object_register_id": objRegId},
            "contact": {
                "standard": [
                    {"salutation": salutation, "required": "true", "datatype": "Text", "regex": ""},
                    {"firstname": firstname, "required": "true", "datatype": "Text", "regex": ""},
                    {"lastname": lastname, "required": "true", "datatype": "Text", "regex": ""},
                    {"email": email, "required": "true", "datatype": "Text", "regex": ""}
                ]
            }
        }
        let formJson = JSON.stringify ( formData );
        let formbtoa = utoa ( formJson );

        $.ajax ( {
            url: "https://dev-api-in-one.net/webhook/receive",
            type: 'POST',
            async: false,
            timeout: 0,
            header: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
                'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
                'Access-Control-Max-Age': '86400'
            },
            data: formbtoa,
            success: function (data) {
                let response = ( data );
                console.log ( response );
                console.log ( response.message );
                console.log ( response.status );
                if ( response.status ) {
                    //$ ( '.btn-submit' ).hide ();
                    $ ( '#nws-submit' ).val ( 'You are successfully subscribed to our Newsletter.' );
                    $ ( '.alert-ex-cus' ).append ( response.message ).addClass ( 'alert-success ' ).removeClass ( 'text-hide' );
                } else {
                    $ ( '#nws-submit' ).val ( 'Something went wrong! Unable subscribe to our Newsletter, Please contact our Customer Care' );
                    $ ( '.alert-ex-cus' ).append ( response.message ).addClass ( 'alert-danger ' ).removeClass ( 'text-hide' );
                }
            }
        } );
        return false;
    } );

} );
