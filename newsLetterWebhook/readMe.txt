WEBHOOK


### FILE STRUCTURE ###

-img/
    -
    -
-scripts/
    - cio_jquery/google/
        - cio_validate_plz.js
        - validate_addr.js
    - google
        - cio_validate_plz.js
        - maps.js
    - jquery/
        - jquery.js
        - jquery.min.js
        - jquery.min.map
        - jquery.slim.js
        - jquery.slim.min.js
        - jquery.slim.min.map
    - cio.js
-styles/
    - bootstrap/
        -
        -
    - css/
        -
        -
-index.php



### INSTALLATION ###
    1. Unzip the folder and check the folder stucture and files as discribed on File sturcture.
    2. Upload the all folders and files to an server.
    3. Refresh the page on the server.
    4. Ensure that all the jquery and file cio.js is loaded.

### INTEGRATION TO EXISTING WEBPAGE ###
    1. Copy the html form present in index.php to existing webpage.
    2. Make sure the existing webpage is using latest jquery.
    3. Make sure file cio.js is also loaded on page load.
    4. Please let us know the mapping structure of your HTML form if the mapping is different from present mapping.
        example:
            <input style="color:#000;background:transparent;" value="" class="btn-outline-dark input-fields form-control postal_code " id="" name="postal_code" placeholder="*Postal Code" type="text"  onpaste="return false;" onCopy="return false;" onCut="return false;">
            This input field has name="postal code" but if the existing webpage has name="plz" or name="zip".

    5. Before testing the webhook,  plese ensure the bellow fields are present.
        index.php        
            <input type="hidden" name="campaign" value="kunden_anmeldung">
            <input type="hidden" name="account" value="1021">
        cio.js
            formData.push({name:"url", value:location.href});
            formData.push({name:"api-key", value:"b261c1b1-4ebf-4801-ad06-3232a164aa6b"});
            
    

        
